const express = require('express');
const router = express.Router();
const controllers = require('../controllers/controllers');

router.post('/', controllers.createFile);
router.get('/', controllers.getFiles);
router.get('/:filename', controllers.getFile);
router.delete('/:filename', controllers.deleteFile);
router.put('/', controllers.updateFile);

module.exports = router;